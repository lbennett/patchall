⚠️ Messy, untested and proven to not work properly yet 😂 ⚠️

# patchall

patch a number of git tags

## Usage

`patchall.js [path_to_formatted_patch]`

```
~/patchall/patchall.js ~/patchall/patches/0001-Fix-issue-running-gitlab-since-puma.patch
```

Use env var `PREVIOUS_MINOR_VERSIONS_COUNT` to set the number of minor versions to target. Defaults to `3`.
For example, if it is currently `v11.6.4`, it will attempt to patch `v11.6.4`, `v11.5.3`, `v11.4.10` and `v11.3.13`.
Assuming those are the latest patches of those versions.

patchall will create a linked worktree for each selected tag and apply the provided patch.

### TODO

patchall *should*:

* correctly apply patches

* result in a clean list of failed patches for the user to check the modifications of each.

* fully commit, push to origin and open an MR targeting the correct stable branch.