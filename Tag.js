const semver = require("semver");

const MINOR = 1;
const PATCH = 2;

class Tag {
  constructor(raw) {
    this.raw = raw;
  }

  rawAsArray() {
    return this.raw.split(".");
  }

  isValid() {
    return semver.valid(this.raw);
  }

  decrementMinor(count = 1) {
    const rawArray = this.rawAsArray();
    rawArray[MINOR] = rawArray[MINOR] - count;
    this.raw = rawArray.join(".");

    return this;
  }

  setPatch(patch) {
    const rawArray = this.rawAsArray();
    rawArray[PATCH] = patch;
    this.raw = rawArray.join(".");

    return this;
  }

  serialize() {
    return semver.coerce(this.raw).version;
  }

  minor() {
    return this.rawAsArray()[MINOR];
  }

  static clone(tag) {
    return new Tag(tag.raw);
  }
}

module.exports = Tag;
