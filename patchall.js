#! /usr/bin/env node

const { exec, spawn } = require("child_process");
const path = require("path");
const TagsCollection = require("./TagsCollection");
const Tag = require("./Tag");

const PREVIOUS_MINOR_VERSIONS_COUNT =
  process.env.PREVIOUS_MINOR_VERSIONS_COUNT || 3;
const PATCH_PATH = process.argv[2];

function applyPatch(tag) {
  const serializedTag = tag.serialize();

  const childProcess = spawn(
    "git",
    ["am", "-3", path.resolve("./", serializedTag, "../", PATCH_PATH)],
    { cwd: path.resolve("./", serializedTag) }
  );

  childProcess.stdout.on("data", data =>
    console.info(serializedTag, data.toString())
  );
  childProcess.stderr.on("data", data =>
    console.error(serializedTag, data.toString())
  );
  childProcess.stdout.on("close", () =>
    console.info(serializedTag, "patch finished.")
  );
}

function linkWorktreesAndApply(tags) {
  tags.forEach(tag => {
    const serializedTag = tag.serialize();

    const childProcess = spawn("git", [
      "worktree",
      "add",
      "-B",
      `patch-${serializedTag}-with-${path.basename(PATCH_PATH)}`,
      serializedTag
    ]);

    childProcess.stdout.on("data", data =>
      console.info(serializedTag, data.toString())
    );
    childProcess.stderr.on("data", data =>
      console.info(serializedTag, data.toString())
    );
    childProcess.stdout.on("close", () => applyPatch(tag));

    return childProcess;
  });
}

function getTagsList() {
  return new Promise((resolve, reject) => {
    exec("git fetch -t && git tag --sort -v:refname", (error, stdout) => {
      if (error) return reject(error);

      resolve(stdout.split("\n"));
    });
  });
}

function getSpecifiedTagsRange(tags) {
  const tagsCollection = TagsCollection.factory(tags);

  const earliestTag = Tag.clone(tagsCollection.latestTag())
    .decrementMinor(PREVIOUS_MINOR_VERSIONS_COUNT)
    .setPatch(0);

  return tagsCollection.latestMinorTagsUntil(earliestTag);
}

getTagsList()
  .then(tags => getSpecifiedTagsRange(tags))
  .then(tagsInRange => linkWorktreesAndApply(tagsInRange));
