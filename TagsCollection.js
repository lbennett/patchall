const semver = require("semver");
const Tag = require("./Tag");

class TagsCollection {
  constructor(tags) {
    this.tags = tags;
  }

  latestTag() {
    return this.tags[0];
  }

  validTags() {
    return this.tags.filter(tag => tag.isValid());
  }

  latestMinorTagsUntil(targetTag) {
    let hasReachedTarget = false;

    return this.tags.reduce((latestMinorTags, tag) => {
      if (hasReachedTarget || !tag) return latestMinorTags;

      const isLaterThanTarget = semver.satisfies(
        tag.serialize(),
        `>=${targetTag.serialize()}`
      );
      const isExistingMinorVersion = latestMinorTags.some(
        latestMinorTag => latestMinorTag.minor() === tag.minor()
      );
      const isValidTag = isLaterThanTarget && !isExistingMinorVersion;

      if (!isLaterThanTarget) hasReachedTarget = true;

      if (!isValidTag) return latestMinorTags;

      latestMinorTags.push(tag);

      return latestMinorTags;
    }, []);
  }

  static factory(rawTags) {
    const tags = rawTags.map(tag => new Tag(tag));

    return new TagsCollection(tags);
  }
}

module.exports = TagsCollection;
